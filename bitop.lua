local bitop = {}
--[[
	pure lua bitop (for lua 5.1) by rnd
  
	Bit words are simple arrays in lua:  word = {0,1,0,0,1,0,1,0}; -- example of 8 bit word
	all functions are accessible from bitop table, like bitop.add for example
  
	List of functions:
	
	bit operations:
		bitand(a,b,res,is_not) -- if is_not then return a and (not b)
		xor(a,b, offset1, offset2, res) -- compute right rotate (to low end) and then xor
		xor_shift(a,b, offset1, offset2, res) -- compute right shift (to low end) and then xor
		add(a,b, res) -- compute a+b modulo 2^n, n = length of word a
    reverse(a) -- reverses array a
	
	conversions:
		dec2bin(dec,bits,res) -- convert decimal number to bit word, low endian (example 4 -> {0,0,1} )
		bin2dec(bin) -- convert bit word back to decimal form, watch out for overflows (max lua integer is ~2^52)
		dec2hex(dec) -- convert decimal form to hex form, output is high endian
		bin2hex(bin)
		str2bin(text) -- convert string to bit word, each byte is converted to 8 bits
--]]


 local dprint = function(a) print(table.concat(a,", ")) end
 --bitop by rnd
 
 local str2bin = function(text)
  local ret = {}
  local bin = {}
  for i = 1,string.len(text) do
      local c = string.byte(text,i) -- 0-255 -> convert to 2 hex
      dec2bin(c,8,bin)
      for j = 1,8 do ret[#ret+1] = bin[j] end
  end
  return ret
end
 
local reverse = function(a) -- reverse digits, in effect change between low/high endian
    local n = #a 
    local m = math.ceil(n/2) 
    
    for i = 1,m do
        local tmp = a[i]
        a[i] = a[n-i+1]
        a[n-i+1] = tmp 
    end
end

local reverse_test = function()
    local a = {1,2,3,4};
    local b = {1,2,3,4,5};
    reverse(a)
    dprint(a)
    reverse(b)
    dprint(b)
end
--reverse_test()

local dec2bin = function(dec,bits,res) -- convert to binary number, low endian
  local x = dec
  local i = 0
  while x>0 and i < bits do
    local c = x%2
    i=i+1 res[i] = c
    x=(x-c)/2
  end
  for j = i+1, bits do res[j] = 0 end
end


local bin2dec = function(bin)
    local bits = #bin
    local res = 0 local b = 1
    for i = 1, bits do
        res = res+b*bin[i]
        b=b*2
    end
    return res
end

local dec2hex = function(dec)
  local x = dec
  local hexd = {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"}
  local i = 0 local ret = {}
  while x>0 do
    local c = x%16
    i=i+1 ret[i] = hexd[c+1]
    x=(x-c)/16
  end
  local out = {}
  for i = 1, #ret do out[i] = ret[#ret-i+1] end
  return table.concat(out,"")
end

local bin2hex = function(bin) 
  local n = #bin  local m = n%4 n=n-m  -- group 4 bits together to get 1 hex digit
  
  local hex = {} 
  for i = 1, n,4 do
    local c = bin[i]+2*bin[i+1]+4*bin[i+2]+8*bin[i+3] 
    if c<10 then hex[#hex+1] = string.char(48+c) else hex[#hex+1] = string.char(97+c-10) end
  end
  
  if m>0 then -- put together the remainder as final digit
    local c = bin[n+1]+2*(bin[n+2] or 0) +4*(bin[n+3] or 0)+8*(bin[n+4] or 0) 
    if c<10 then hex[#hex+1] = string.char(48+c) else hex[#hex+1] = string.char(97+c-10) end
  end
  
  reverse(hex)  -- result is usually big endian
  return table.concat(hex,"")
end

-- right rotate rotates bits to low end ( = to the right in big endian ).
-- in code: rotate(a,offset1)[i] = a[(i+offset) % length], if indexes start with 0

local xor = function(a,b, offset1, offset2, res) -- compute right rotate (to low end) and then xor
    local n = #a 
    for i = 1,n do
        res[i] = (a[(i+offset1-1)%n+1]==b[(i+offset2-1)%n+1]) and 0 or 1
    end
end

local xor_test = function()
  local a = {1,1,0,0,0}  -- 1 0 0
  local b = {1,1,1,0,0,0} --  1 0 0
  local res = {}
  offset1 = 1; offset2 = 1;
  
  local n = #a;
  local ret = {};
  
  print("ROTATE TEST rotate_right(a, offset1)")
  for i = 1,n do ret[i] = a[(i+offset1-1)%n+1] end; dprint(ret)
  
  print("XOR test")
  xor(a,b, offset1, offset2, res)
  print("res"); dprint(res)
end
--xor_test() -- OK

local xor_shift = function(a,b, offset1, offset2, res) -- compute right shift (to low end) and then xor
    -- 0 beyond an-offset1 and 0 beyond bn-offset2
    for i = 1,#a do
        res[i] = ((a[i+offset1] or 0) == (b[i+offset2] or 0)) and 0 or 1
    end
end

local xor_shift_test = function()
  local a = {1,1,0,1,1}  -- 1 0 0
  local b = {1,1,1,1,1,1} --  1 0 0
  local res = {}
  offset1 = 4; offset2 = 1;
  
  local n = #a;
  local ret = {};
  print("SHIFT test right_shift(a, offset1)")
  for i = 1,n do ret[i] = a[i+offset1] or 0 end; dprint(ret)
  
  xor_shift(a,b, offset1, offset2, res)
  print("res"); dprint(res)
end
--xor_shift_test() -- OK
 
local add = function(a,b,res) -- res = (a+b) % 2^bits = (a+b - 2^ bits = a+b with (bits+1) bit dropped )
  local carry = 0
  for i = 1,#a do
    local c = a[i]+b[i]+ carry
    if c>1 then carry = 1 c = c-2 else carry = 0 end
    res[i] = c
  end
end


local add_test = function()
    local ad = 175
    local bd = 141
    local a = {} local b = {}
    local bits = 8
    dec2bin(ad,bits, a);dec2bin(bd,bits, b)
    dprint(a)
    dprint(b)
    local res = {};
    add(a,b,res)
    dprint(res)
    print("(ad + bd) % 2^bits = " .. (ad+bd) % 2^bits .. " ?= res = " .. bin2dec(res))
end
--add_test() -- OK

local bitand = function(a,b,res,is_not) -- if is_not then return a and (not b)
    if is_not then
      for i=1,#a do res[i] = (a[i]==1 and b[i]==0) and 1 or 0 end
    else
      for i=1,#a do res[i] = (a[i]==1 and b[i]==1) and 1 or 0 end
    end
end

local tablecopy = function(a,b) for i = 1,#a do b[i]=a[i] end end -- can not just do b=a cause table value is actually pointer

bitop = {bitand = bitand, xor = xor, xor_shift = xor_shift, add = add, dec2bin = dec2bin, bin2dec = bin2dec, dec2hex = dec2hex, bin2hex = bin2hex, str2bin = str2bin, reverse = reverse}

return bitop