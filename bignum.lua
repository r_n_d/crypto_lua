-- BIGNUM by rnd v05122018b
-- functions: 
--	new, tostring, rnd, import/exportdec,import/exporthex _add, _sub, mul, div2, div, is_larger, is_equal, add, sub,
--	binary2base, base2binary
--	bignum.barrett, bignum.mod, bignum.modpow

bignum = {};
bignum.new = function(base,sgn, digits)
	local ret = {};
	ret.base = base -- base of digit system
	ret.digits = {};
	ret.sgn = sgn -- sign of number,+1 or -1
	local data = ret.digits;
	local m = #digits;
	ret.digits = digits; 
	--for i=1,m do data[i] = digits[m-i+1] end -- copy
	return ret
end

bignum.tablecopy = function(data) -- make copy cause tables are passed by reference
	local ret = {};
	for i = 1,#data do ret[i]=data[i] end
	return ret
end

--TODO: safe random
bignum.rnd = function(base,sgn, length) -- random number
	local ret = {};
	for i =1,length do ret[#ret+1] = math.random(base)-1 end
	return bignum.new(base,sgn,ret)
end

bignum.tostring = function(n)
	local ret =  {};
	for i = #n.digits,1,-1 do ret[#ret+1] = n.digits[i] end
	return (n.sgn>0 and "" or "-") .. table.concat(ret,"'") .. "_" ..n.base
end

--n1 = bignum.new(10,-1,{5,7,3,1})
--say(bignum.tostring(n1))

bignum.importdec = function(ndec)
	local ret = {};
	local sgn = ndec>0 and 1 or -1;
	local base = 10;
	local n = ndec*sgn;
	local data = {};
	while n>0 do
		local r = n%base
		data[#data+1] = r;
		n=(n-r)/base
	end
	ret.base = base; ret.sgn = sgn; ret.digits = data;
	return ret
end

bignum.exportdec = function(n) -- warning: can cause overflow if number larger than 2^52 ~ 4.5*10^15
	local ndec = 0;
	for i = #n.digits,1,-1 do ndec = 10*ndec + n.digits[i] end
	return ndec*n.sgn
end

bignum.importhex = function(hex) -- nhex is string with characters 0-9(48-57) and a-f(97-102)
	local ret = {sgn=1,base = 16, digits = {}};
	local data = ret.digits;
	local length = string.len(hex);
	for i = length,1,-1 do
		local c = string.byte(hex,i)
		if c>=48 and c<=57 then
			data[length-i+1] = c-48
		elseif c>=97 and c<=102 then
			data[length-i+1]=c-97+10
		end
	end
	return ret
end

bignum.exporthex = function(nhex) -- returns string with hex
	if nhex.base~=16 then return end
	local data = nhex.digits;
	local ret = {};
	for i = #data,1,-1 do
		local c = data[i];
		if c<10 then ret[#ret+1] =  string.char(48+c) else ret[#ret+1] =  string.char(97+c-10) end
	end
	return table.concat(ret,"")
end

local base64c = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
"1","2","3","4","5","6","7","8","9","0","+","/"}
local base64n = {}; for i=1,#base64c do	base64n[base64c[i]]=i-1 end

bignum.exportbase64 = function(n)
	local binary = bignum.base2binary(n);
	local base64 = bignum.binary2base(binary,64);
	local data = base64.digits;	local ret = {};
	for i = #data,1,-1 do ret[#data-i+1] = base64c[data[i]+1] end
	return table.concat(ret,"")
end

bignum.importbase64 = function(base64,newbase)
	local digits = {}; local length = string.len(base64);
	for i = length,1,-1 do digits[length-i+1] = base64n[string.sub(base64,i,i)]	end
	local base64 = bignum.new(64,1,digits);
	local binary = bignum.base2binary(base64);
	return bignum.binary2base(binary,newbase);
end

bignum.importascii = function(text,newbase)
	local m = 32; -- ascii 32-127
	local digits = {}; local j = 1;
	for i=1, string.len(text) do 
		local c = string.byte(text,i)-m;
		if c>=0 and c<96 then digits[j] = c; j =j +1 end
	end
	local ascii = bignum.new(96,1,digits);
	local binary = bignum.base2binary(ascii);
	return bignum.binary2base(binary,newbase);
end


bignum.exportascii = function(n) -- 32 - 127 (96) -- problem with non even base
	local binary = bignum.base2binary(n);
	local ascii = bignum.binary2base(binary,96);
	local out = ascii.digits;
	local m = 32;local ret = {}; 
	for i = 1, #out do
		ret[#ret+1] = string.char(m+out[i])
	end
	return table.concat(ret,"")
end

-----------------------------------------------
--	ADDITION
-----------------------------------------------


bignum._add = function(n1,n2,res) -- assume both >0, same base: n1+n2 -> res
	local b = n1.base;
	local m1 = #n1.digits;
	local m2 = #n2.digits
	local m = m1; if m2<m then m = m2 end
	local M = m1;if m2>M then M = m2 end
	
	local data1 = n1.digits; local data2 = n2.digits;
	res.digits = {} -- expensive?
	local data = res.digits; local carry = 0;
	for i = 1,M do
		local j = (data1[i] or 0) +(data2[i] or 0) + carry;
		if j >=b then carry = 1; j = j-b else carry = 0 end
		data[i] = j
	end
	if carry== 1 then data[M+1] = 1 end
	res.base = n1.base
end


-----------------------------------------------
--	SUBTRACTION
-----------------------------------------------

bignum._sub = function(n1,n2,res) -- assume n1>n2>0, same base: n1-n2 -> res
	local b = n1.base;
	local m1 = #n1.digits;
	local m2 = #n2.digits
	local m = m1; if m2<m then m = m2 end
	local M = m1;if m2>M then M = m2 end
	
	local data1 = n1.digits; local data2 = n2.digits;
	res.digits = {};
	local data = res.digits; local carry = 0;
	local maxi = 0;
	for i = 1,M do
		local j = (data1[i] or 0) - (data2[i] or 0) + carry;
		if j < 0 then carry = -1; j = j+b else carry = 0 end
		if j~=0 then maxi = i end -- max nonzero digit
		data[i] = j
	end
	
	for i = maxi+1,M do	data[i] = nil end -- remove trailing zero digits if any
	res.base = n1.base
end


bignum.is_equal = function(n1,n2) -- assume both >0, same base. return true if n1==n2
	local b = n1.base;
	local data1 = n1.digits; local data2 = n2.digits;
	if #data1~=#data2 then return false end
	for i =#data1,1,-1 do -- from high bits
		local d1 = data1[i];
		local d2 = data2[i];
		if d1~=d2 then return false end
	end
	return true -- all digits were ==
end

bignum.is_larger = function(n1,n2) -- assume both >0, same base. return true if n1>=n2
	local b = n1.base;
	local data1 = n1.digits; local data2 = n2.digits;
	if #data1>#data2 then return true elseif #data1<#data2 then return false end
	--remains when both same lentgth
	for i =#data1,1,-1 do -- from high bits
		local d1 = data1[i];
		local d2 = data2[i];
		if d1>d2 then return true elseif d1<d2 then return false end
	end
	return true -- all digits were >=, still larger
end

bignum.add = function(n1,n2,res) -- handle all cases, >0 or <0
	local sgn1 = n1.sgn;
	local sgn2 = n2.sgn;
	if sgn1*sgn2>0 then bignum._add(n1,n2,res); res.sgn = sgn1; return end -- simple case
	
	local is_larger = bignum.is_larger(n1,n2) -- is abs(n1)>abs(n2) ?
	local sgn = 1;
	if is_larger then sgn = sgn1 else sgn = sgn2 end
	
	if is_larger then 
		bignum._sub(n1,n2,res);
	else
		bignum._sub(n2,n1,res);
	end
	res.sgn = sgn
end

bignum.sub = function(n1,n2,res) -- handle all cases, >0 or <0
	--just add(n1,-n2)
	local sgn1 = n1.sgn;
	local sgn2 = -n2.sgn;
	if sgn1*sgn2>0 then bignum._add(n1,n2,res); res.sgn = sgn1; return end -- simple case
	
	local is_larger = bignum.is_larger(n1,n2) -- is abs(n1)>abs(n2) ?
	local sgn = 1;
	if is_larger then sgn = sgn1 else sgn = sgn2 end
	
	if is_larger then 
		bignum._sub(n1,n2,res);
	else
		bignum._sub(n2,n1,res);
	end
	res.sgn = sgn
end


-----------------------------------------------
--	MULTIPLY 
-----------------------------------------------

bignum.mul = function(n1,n2,res)
	
	local base = n1.base
	local sgn = n1.sgn*n2.sgn;
	
	local data1 = n1.digits; local m1 = #data1;
	local data2 = n2.digits; local m2 = #data2;
	
	res.digits = {}; res.base = base
	local data = res.digits; local m = m1+m2;
	
	local carry = 0
	for i = 1, m1 do
		-- multiply i-th digit of data1 and add to res
		local d1 = data1[i];
		carry  = 0
		for j = 1,m2 do
			local d2 = data2[j];
			local d = carry + d1*d2;
			local r =  (data[i+j-1] or 0) + d
			if r>=base then 
				data[i+j-1] = r % base; carry = (r - (r%base))/base 
			else 
				data[i+j-1] = r; carry = 0
			end
		end
		if carry>0 then data[i+m2] = carry % base end
	end
end


-- m = 300, base 2^26, 100 repeats: amd ryzen 1200: 0.1s, amd-e350 apu 1.6ghz (2010) : 5.15s
mul_bench = function()
	local m = 300;
	local base = math.floor(2^26) -- careful! lua 5.3 computes this with .0, lua 5.1 as integer
	local r = 100
	
	local n1 = bignum.rnd(base, 1, m)
	local n2 = bignum.rnd(base, 1, m)
	local res = {digits = {}};
	local t = os.clock()
	for i = 1, r do	bignum.mul(n1,n2,res) end
	local elapsed = os.clock() - t;
	--say("n1 = " .. bignum.tostring(n1) .. ", n2 = " .. bignum.tostring(n2))
	say("mul benchmark. ".. m .. " digits, base " .. base .. ", repeats " .. r ..  " -> time " .. elapsed)
end
--mul_bench()


-----------------------------------------------
--	DIVIDE
-----------------------------------------------

bignum.div2 = function(n,res) -- res = n/2, return n % 2. note: its safe to do: bignum.div2(res,res);
	
	local base = n.base;
	local data = n.digits; local m = #data;
	
	res.digits = {};
	local rdata = res.digits;
	local carry = 0
	
	local q = data[m]/2; 
	local fq = math.floor(q);
	if q~=fq then carry = base end
	if fq>0 then rdata[m] = fq else rdata[m]=nil end -- maybe digits shrink by 1?
	
	for i = m-1,1,-1 do
		local q = (data[i]+carry)/2;
		local fq = math.floor(q)
		if q~= fq then carry = base else carry = 0 end
		rdata[i] = fq;
	end
	if carry ~= 0 then return 1 else return 0 end
end

bignum.div = function(N,D, res) -- res = [N/D]
	
	local base = N.base;
	res.base = base
	res.digits = {}; 
	local data = res.digits; 
	
	local n1 = #N.digits;local n2 = #D.digits;
	-- trivial cases, prevent wasting time here
	if n1<n2 then res.digits = {0}; return end -- clearly N<D
	if n2 == 1 and D.digits[1] == 1 then res.digits = N.digits return end -- division by 1!
	
	local low = bignum.new(base,1,{})
	local high = bignum.new(base,1,{})
	-- better initial range for less needed iterations
	local ldigits = low.digits;local hdigits = high.digits;
	for i = 1,n1-n2 do ldigits[i]=0;hdigits[i]=0 end
	ldigits[n1-n2]=N.digits[n1];hdigits[n1-n2+1] = ldigits[n1-n2];
	--say("low " .. bignum.tostring(low) .. " high " .. bignum.tostring(high))
	
	local mid = bignum.new(base,1,{});
	local temp = bignum.new(base,1,{});
	local step = 0;
	
	while step < 100000 do -- in practice this uses around log_2 (base^(n2-n1)) iterations, for example dividing 8192 bit number by 4096 takes ~4000 iterations..
		step = step + 1
		bignum._add(low,high,mid); bignum.div2(mid,mid); -- mid = (low+high)/2
		
		if bignum.is_equal(low,mid) then 
			if DEBUG then say("DONE. step  " .. step) end-- .. " low = " .. bignum.tostring(low) .. " high = " .. bignum.tostring(high) .. " mid = " .. bignum.tostring(mid))
			res.digits = mid.digits
			return
		end

		bignum.mul(D, mid, temp) -- temp = D*mid
		if bignum.is_larger(N,temp) then low.digits = mid.digits else high.digits = mid.digits end
	end
end

bignum.base2binary = function(_n)
	local base = _n.base;
	local n = {sgn = 1, base = base, digits = _n.digits }
	local data = n.digits;
	local i = 0;
	local out =  {};
	while (#data > 1 or (#data==1 and data[1] > 0)) do -- n>0
		i=i+1
		out[i]  = bignum.div2(n,n); data = n.digits;
	end
	return {sgn=1,base = 2, digits = out}
end

bignum.binary2base = function(n, newbase) -- newbase must be even
	local base = n.base;
	local ret = {sgn=1,base=newbase, digits = {0}}
	local out = ret.digits
	local data = n.digits
	for i = #data,1,-1 do
		bignum._add(ret,ret,ret) -- ret = 2*ret
		out = ret.digits
		out[1]=out[1]+ data[i]; -- if newbase is even no carry, else more complication here
	end
	return ret
end


-----------------------------------------------
--	MODULAR MULTIPLY
-----------------------------------------------

bignum.get_barrett = function(n) 
	local base = n.base;
	local k = 2*#n.digits+2; -- n<B^(n1+1) -> k = 2*(n1+1)
	local Bk = bignum.new(base,1,{})
	local res = bignum.new(base,1,{})
	local data = Bk.digits;
	for i =1,k do data[i]= 0 end; data[k+1]=1; -- this is B^k
	bignum.div(Bk, n,res);
	return {n=n, m=res, k=k};
end

-- mod using barrett. possible improvement: montgomery.
bignum.mod = function(a,barrett,res) -- a should be less or equal (n-1)^2, stores a%n into res
	
	local k = barrett.k;
	local n = barrett.n;
	local m = barrett.m;
	local base = a.base;
	
	bignum.mul(a,m,res); -- large multiply 1: res = a*m

	local data = res.digits;local n1 = #data;  --res = res / B^k
	for i = 1, n1-k do data[i]=data[i+k] end; for i = n1-k+1,n1 do data[i] = nil end -- bitshift
	
	local temp = bignum.new(base,1,{});
	bignum.mul(res,n, temp); -- multiply 2: res*n
	bignum._sub(a,temp,res); -- subtract: res = a - res*n
	if bignum.is_larger(res,n) then bignum._sub(res,n,res) end
end


bignum.modpow = function(a_,b_,barrett) -- efficiently calculate a^b mod n, need log_2 b steps
	local base = a_.base
	local a = bignum.new(base,1,a_.digits); -- base
	local b = bignum.new(base,1,b_.digits); -- exponent
	local bdata = b.digits;
	
	local ret = bignum.new(base,1,{1});
	local temp = bignum.new(base,1,{});
	
	while (#bdata > 1 or (#bdata==1 and bdata[1] > 0)) do -- b>0
		if bdata[1] % 2 == 1 then
			bignum.mul(ret, a, temp) 
			bignum.mod(temp,barrett, ret) -- ret = a*ret % n
		end
		bignum.div2(b,b); bdata = b.digits -- b = b/2
		
		bignum.mul(a,a,temp);bignum.mod(temp,barrett, a) -- a=a^2 % n
	end

	--local dig_ = ret.digits; for i = 1,#dig_ do dig_[i] = math.floor(dig_[i]) end --fix for boxface 'lua' :)
	return ret
end