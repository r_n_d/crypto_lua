-- sha256 by rnd
-- global function sha256(text)

--NOTE: sha256("") does not compute correctly, 
-- even when all bit functions  work correctly (xor with rotate/shift, bitand, add)

--ALGORITHM OVERVIEW:
-- 1.divide data  into 512 bit blocks (pad with 1 bit '1'  and then 0 to get correct size
-- 2. for each block first derive 64 values (using 8 32 bit from 256 block) using mixing with rot and xor and +
-- 3. compress the large 64 value array into 8 values using mixing similiar to 2, here initial value is taken to be resulting hash of previous blocks so far
-- 4. add resulted 8 bit words to final hash value ( just +)
 
-- 'Merkle-Damgard construction':
-- When we add another 256 bit block the resulting hash value is mixed with hash value obtained from previous blocks:
-- h_(i+1) = h_i  + block_hash_(i+1)(h_i), i = 0,...7. This can be abused for length extension attacks, ability to compute
-- hash with new data added without knowing the previous data
 
-- Solution to length extension is just use bigger state and discard some data from output.
 
--bitop by rnd  
local path = string.gsub(debug.getinfo(1).short_src, "^(.+\\)[^\\]+$", "%1") -- very neat and good way to get current file path, much better than 'working' require('bitop') with bitop.lua in SAME folder as sha256.lua </sarcasm>

bitop = dofile(path.."bitop.lua")
local dec2bin = bitop.dec2bin; local reverse = bitop.reverse; local xor = bitop.xor
local xor_shift = bitop.xor_shift; local add = bitop.add; local bitand = bitop.bitand
local bin2hex = bitop.bin2hex; local dout = function(a) return table.concat(a,"") end

local tablecopy = function(a,b) for i = 1,#a do b[i]=a[i] end end -- can not just do b=a cause table value is actually pointer

--Note 1: All variables are 32 bit unsigned integers and addition is calculated modulo 2^32
--Note 2: For each round, there is one round constant k[i] and one entry in the message schedule array w[i], 0 ≤ i ≤ 63
--Note 3: The compression function uses 8 working variables, a through h
--Note 4: Big-endian convention is used when expressing the constants in this pseudocode,
--    and when parsing message block data from bytes to words, for example,
--    the first word of the input message "abc" after padding is 0x61626380

local sha256_ = function(input)
  local msg = {} tablecopy(input,msg)
  
--  Initialize hash values:
--  (first 32 bits of the fractional parts of the square roots of the first 8 primes 2..19):
--  2,3,5,7,11,13,17,19

  local h0 = {}local h1 = {}local h2 = {}local h3 = {}
  local h4 = {}local h5 = {}local h6 = {}local h7 = {}

  dec2bin(0x6a09e667,32,h0) 
  dec2bin(0xbb67ae85,32,h1)
  dec2bin(0x3c6ef372,32,h2)
  dec2bin(0xa54ff53a,32,h3)
  
  dec2bin(0x510e527f,32,h4)
  dec2bin(0x9b05688c,32,h5)
  dec2bin(0x1f83d9ab,32,h6)
  dec2bin(0x5be0cd19,32,h7)
  
  -- constants are in 'big endian' so convert to 'low endian' we use
  reverse(h0) reverse(h1) reverse(h2) reverse(h3) 
  reverse(h4) reverse(h5) reverse(h6) reverse(h7) 

  --Initialize array of round constants:
  --(first 32 bits of the fractional parts of the cube roots of the first 64 primes 2..311):
    local kd =  -- [0,63]
   {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
   0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
   0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
   0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
   0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
   0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
   0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
   0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2}

  local k = {}for i = 1,#kd do k[i] = {} dec2bin(kd[i],32,k[i]) reverse(k[i]) end
  
  --Pre-processing (Padding):
--  begin with the original message of length L bits
--  append a single '1' bit
--  append K '0' bits, where K is the minimum number >= 0 such that L + 1 + K + 64 is a multiple of 512
--  append L as a 64-bit big-endian integer, making the total post-processed length a multiple of 512 bits
  
  
  local L = #msg
  local Lb = {} dec2bin(L,64,Lb); reverse(Lb);
  local K = (L+65) % 512 if K>0 then K = 512-K end -- this is how data is organized: [msg 1 0..0 L]
  L = L + 65+K -- new length, divisible by 512
  msg[#msg+1 ] = 1 -- 1 bit added [msg 1]
  for i = 1,K do msg[#msg+1] = 0 end
  K = #Lb for i=1,K do msg[#msg+1] = Lb[K-i+1] end
  local chunks = L / 512
  
  print("DEBUG input message : " .. dout(input)) 
  print("DEBUG initial message (512 bit block after postprocessing) : " .. dout(msg))
  print("DEBUG length of postprocessed message : " .. #msg)
  
--  Process the message in successive 512-bit chunks:
--  break message into 512-bit chunks
--  for each chunk
--      create a 64-entry message schedule array (c notation w[0..63]) of 32-bit words
--      (The initial values in w[0..63] don't matter, so many implementations zero them here)

  local w = {}
  local a = {}local b = {}local c = {}local d = {}
  local e = {}local f = {}local g = {}local h = {}

  for i = 1,64 do w[i] = {} end

  for chunk = 1, chunks do
      for i = 1,16 do
        for j = 1,32 do 
          w[i][j] = msg[(chunk-1)*512+32*(i-1)+j] -- copy 512 bit chunk into first 16 words of the message schedule array (c notation w[0..15])
        end 
      end
      
      --Extend the first 16 words into the remaining 48 words w[16..63] of the message schedule array(17...64 with idx start 1)
      local s0 = {}local s1 = {}
      for i = 17,64 do
        xor(w[i-15],w[i-15], 7,18,s0) xor_shift(s0,w[i-15],0,3,s0)    -- s0 := (w[i-15] rightrotate 7) xor (w[i-15] rightrotate 18) xor (w[i-15] rightshift 3)
        xor(w[i-2], w[i-2], 17,19,s1) xor_shift(s1, w[i-2], 0,10,s1) -- s1 := (w[i-2] rightrotate 17) xor (w[i-2] rightrotate 19) xor (w[i-2] rightshift 10)
        add(s0,s1,s1)
        add(s1,w[i-16],s1)
        add(s1,w[i-7],w[i]) --w[i] := w[i-16] + s0 + w[i-7] + s1
      end
      
      --Initialize working variables to current hash value:
      tablecopy(h0,a)
      tablecopy(h1,b)
      tablecopy(h2,c)
      tablecopy(h3,d)
      tablecopy(h4,e)
      tablecopy(h5,f)
      tablecopy(h6,g)
      tablecopy(h7,h)
        
      --  Compression function main loop:
      local S0 = {} local S1 = {}local ch ={} local tmp = {} local temp1 = {} local maj = {} local temp2 = {}
      for i=1,64 do 
        xor(e,e,6,11,S1) xor(S1,e,0,25,S1) --S1 := (e rightrotate 6) xor (e rightrotate 11) xor (e rightrotate 25)
        bitand(e,f,tmp) bitand(g,e,ch,true) xor(tmp,ch,0,0,ch) --ch := (e and f) xor ((not e) and g)
        add(h,S1,tmp) add(tmp,ch,tmp) add(tmp,k[i],tmp) add(tmp,w[i],temp1) --temp1 := h + S1 + ch + k[i] + w[i]
        xor(a,a,2,13,S0) xor(S0,a,0,22,S0) --S0 := (a rightrotate 2) xor (a rightrotate 13) xor (a rightrotate 22)
        bitand(a,b,tmp) bitand(a,c,temp2) xor(tmp,temp2,0,0,tmp) bitand(b,c,temp2) xor(tmp,temp2,0,0,maj) --maj := (a and b) xor (a and c) xor (b and c)
        add(S0,maj,temp2) --temp2 := S0 + maj

        tablecopy(g,h) --h := g
        tablecopy(f,g) --g := f
        tablecopy(e,f) --f := e
        add(d,temp1,e) --e := d + temp1
        tablecopy(c,d) --d := c
        tablecopy(b,c) --c := b
        tablecopy(a,b) --b := a
        add(temp1,temp2,a) --a := temp1 + temp2
      end
      
      --Add the compressed chunk to the current hash value:
      add(h0,a,h0) --h0 := h0 + a
      add(h1,b,h1)--h1 := h1 + b
      add(h2,c,h2)--h2 := h2 + c
      add(h3,d,h3)--h3 := h3 + d
      add(h4,e,h4)--h4 := h4 + e
      add(h5,f,h5)--h5 := h5 + f
      add(h6,g,h6)--h6 := h6 + g
      add(h7,h,h7)--h7 := h7 + h
  end
  
  --reverse(h0) reverse(h1) reverse(h2) reverse(h3) 
  --reverse(h4) reverse(h5) reverse(h6) reverse(h7) 
  
  return bin2hex(h0) .. bin2hex(h1) .. bin2hex(h2) .. bin2hex(h3) ..  bin2hex(h4) .. bin2hex(h5) .. bin2hex(h6) .. bin2hex(h7)
end

local str2bin = function(text)
  local ret = {}
  local bin = {}
  for i = 1,string.len(text) do
      local c = string.byte(text,i) -- 0-255 -> convert to 2 hex
      dec2bin(c,8,bin)
      for j = 1,8 do ret[#ret+1] = bin[j] end
  end
  return ret
end

sha256 = function(text)
  return sha256_(str2bin(text))
end

--sha256("")= e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
-- fail, 1ba0dfe7bf2dfd626ccd1f8ab1268b1fc6db084de4902c6b3e0f298cbe7fe6a8, but why?

print(sha256(""))
--print(sha256("a"))